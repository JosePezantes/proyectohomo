'use strict';
var models = require('../models/');
const Sequelize = require('sequelize');
var Persona = models.persona;
var Tramite = models.tramite;
var Seguimiento = models.seguimiento;
var Carrera = models.carrera;
var Docente = models.docente;
var Archivo = models.archivo;
/**
 * Clase que permite manipular los datos del modelo persona, tramite, seguimiento, archivo, carrera, docente
 */
class controladorSeguimiento {
    /**
     * Funcion que permite visualizar los seguimiento de los tramites echos
     * @param {type} req el objeto peticion
     * @param {type} res el objeto respuesta
     * @returns {undefined} redirecciona a la pagina /menu/seguimiento
     */
    visualizarSeguimiento(req, res) {
        if (req.user.rolN === "Decano") {
            var Lista = {};
            Carrera.findAll({}).then(function (carrera) {
                if (carrera) {
                    Docente.findAll({include: {model: Persona}}).then(function (docente) {
                        if (docente) {
                            Seguimiento.findAll({include: {model: Tramite, include: [{model: Persona}, {model: Archivo}]}}).then(function (solicitudes) {
                                if (solicitudes) {
                                    solicitudes.forEach(function (solicitudes, i) {
                                        carrera.forEach(function (lcarrera, j) {
                                            docente.forEach(function (ldocente, k) {
                                                if (solicitudes.tramite.external_docente === ldocente.external_id && solicitudes.tramite.external_carrera === lcarrera.external_id) {
                                                    Lista[i] = {
                                                        registro: solicitudes.tramite.registro,
                                                        solicitante: solicitudes.tramite.persona.nombres + " " + solicitudes.tramite.persona.apellidos,
                                                        revSolicitud: solicitudes.revSolicitud,
                                                        revSellos: solicitudes.revSellos,
                                                        asigDocente: solicitudes.asigDocente,
                                                        revDocumentacion: solicitudes.revDocumentacion,
                                                        estado: solicitudes.tramite.estado,
                                                        tipo: solicitudes.tramite.tipo,
                                                        cedula: solicitudes.tramite.persona.cedula,
                                                        external_id: solicitudes.external_id,
                                                        tramite: solicitudes.tramite.external_id,
                                                        carrera: lcarrera.nombre,
                                                        archivo: solicitudes.tramite.archivo.archivo,
                                                        cedula_docente: ldocente.persona.cedula,
                                                        docente: ldocente.persona.nombres + " " + ldocente.persona.apellidos,
                                                        foto: solicitudes.tramite.persona.foto
                                                    };
                                                }
                                            });

                                        });
                                    });
                                    //res.send(Lista);
                                    // res.send(seguimiento);
                                    res.render('partials/decano/index',
                                            {titulo: "Seguimiento",
                                                partial: 'decano/frm_seguimiento',
                                                nombre: req.user.nom,
                                                rol: req.user.rolN,
                                                foto: req.user.foto,
                                                idPersona: req.user.idPersona,
                                                Seguimiento: Lista,
                                                msg: {
                                                    error: req.flash('error'),
                                                    info: req.flash("info")
                                                }
                                            });
                                }
                            }).catch(function (err) {
                                req.flash('error', 'Hubo un error' + err);
                                res.redirect('/');
                            });
                        }

                    }).catch(function (err) {
                        req.flash('info', 'Hubo un error');
                        res.redirect('/');
                    });
                }

            }).catch(function (err) {
                req.flash('info', 'Hubo un error');
                res.redirect('/');
            });
        } else if (req.user.rolN === "Abogado") {
            var Lista = {};
            Carrera.findAll({}).then(function (carrera) {
                if (carrera) {
                    Docente.findAll({include: {model: Persona}}).then(function (docente) {
                        if (docente) {
                            Seguimiento.findAll({include: {model: Tramite, include: [{model: Persona}, {model: Archivo}]}}).then(function (solicitudes) {
                                if (solicitudes) {
                                    solicitudes.forEach(function (solicitudes, i) {
                                        carrera.forEach(function (lcarrera, j) {
                                            docente.forEach(function (ldocente, k) {
                                                if (solicitudes.tramite.external_docente === ldocente.external_id && solicitudes.tramite.external_carrera === lcarrera.external_id) {
                                                    Lista[i] = {
                                                        registro: solicitudes.tramite.registro,
                                                        solicitante: solicitudes.tramite.persona.nombres + " " + solicitudes.tramite.persona.apellidos,
                                                        revSolicitud: solicitudes.revSolicitud,
                                                        revSellos: solicitudes.revSellos,
                                                        asigDocente: solicitudes.asigDocente,
                                                        revDocumentacion: solicitudes.revDocumentacion,
                                                        estado: solicitudes.tramite.estado,
                                                        tipo: solicitudes.tramite.tipo,
                                                        cedula: solicitudes.tramite.persona.cedula,
                                                        external_id: solicitudes.external_id,
                                                        tramite: solicitudes.tramite.external_id,
                                                        carrera: lcarrera.nombre,
                                                        archivo: solicitudes.tramite.archivo.archivo,
                                                        cedula_docente: ldocente.persona.cedula,
                                                        docente: ldocente.persona.nombres + " " + ldocente.persona.apellidos,
                                                        foto: solicitudes.tramite.persona.foto
                                                    };
                                                }
                                            });

                                        });
                                    });
                                    //res.send(Lista);
                                    // res.send(seguimiento);
                                    res.render('partials/abogado/index',
                                            {titulo: "Seguimiento",
                                                partial: 'abogado/frm_seguimiento',
                                                nombre: req.user.nom,
                                                rol: req.user.rolN,
                                                foto: req.user.foto,
                                                idPersona: req.user.idPersona,
                                                Seguimiento: Lista,
                                                msg: {
                                                    error: req.flash('error'),
                                                    info: req.flash("info")
                                                }
                                            });
                                }
                            }).catch(function (err) {
                                req.flash('error', 'Hubo un error' + err);
                                res.redirect('/');
                            });
                        }
                    }).catch(function (err) {
                        req.flash('info', 'Hubo un error');
                        res.redirect('/');
                    });
                }

            }).catch(function (err) {
                req.flash('info', 'Hubo un error');
                res.redirect('/');
            });
        } else if (req.user.rolN === "Docente") {
            var Lista = {};
            Carrera.findAll({}).then(function (carrera) {
                if (carrera) {
                    Docente.findOne({include: {model: Persona, where: {id: req.user.idPersona}}}).then(function (docente) {
                        if (docente) {
                            Seguimiento.findAll({include: {model: Tramite, include: [{model: Persona}, {model: Archivo}]}}).then(function (solicitudes) {
                                if (solicitudes) {
                                    solicitudes.forEach(function (solicitudes, i) {
                                        carrera.forEach(function (lcarrera, j) {

                                            if (solicitudes.tramite.external_docente === docente.external_id && solicitudes.tramite.external_carrera === lcarrera.external_id) {
                                                Lista[i] = {
                                                    registro: solicitudes.tramite.registro,
                                                    solicitante: solicitudes.tramite.persona.nombres + " " + solicitudes.tramite.persona.apellidos,
                                                    revSolicitud: solicitudes.revSolicitud,
                                                    revSellos: solicitudes.revSellos,
                                                    asigDocente: solicitudes.asigDocente,
                                                    revDocumentacion: solicitudes.revDocumentacion,
                                                    estado: solicitudes.tramite.estado,
                                                    tipo: solicitudes.tramite.tipo,
                                                    cedula: solicitudes.tramite.persona.cedula,
                                                    external_id: solicitudes.external_id,
                                                    tramite: solicitudes.tramite.external_id,
                                                    carrera: lcarrera.nombre,
                                                    archivo: solicitudes.tramite.archivo.archivo,
                                                    cedula_docente: docente.persona.cedula,
                                                    docente: docente.persona.nombres + " " + docente.persona.apellidos,
                                                    foto: solicitudes.tramite.persona.foto
                                                };
                                            }


                                        });
                                    });
                                    //res.send(Lista);
                                    // res.send(seguimiento);
                                    res.render('partials/docente/index',
                                            {titulo: "Seguimiento",
                                                partial: 'docente/frm_seguimiento',
                                                nombre: req.user.nom,
                                                rol: req.user.rolN,
                                                foto: req.user.foto,
                                                idPersona: req.user.idPersona,
                                                Seguimiento: Lista,
                                                msg: {
                                                    error: req.flash('error'),
                                                    info: req.flash("info")
                                                }
                                            });
                                }
                            }).catch(function (err) {
                                req.flash('error', 'Hubo un error' + err);
                                res.redirect('/');
                            });
                        }

                    }).catch(function (err) {
                        req.flash('info', 'Hubo un error');
                        res.redirect('/');
                    });
                }

            }).catch(function (err) {
                req.flash('info', 'Hubo un error');
                res.redirect('/');
            });
        } else if (req.user.rolN === "Solicitante") {
            var Lista = {};
            Carrera.findAll({}).then(function (carrera) {
                if (carrera) {
                    Docente.findAll({include: {model: Persona}}).then(function (docente) {
                        if (docente) {
                            Seguimiento.findAll({include: {model: Tramite, where: {id_persona: req.user.idPersona}, include: [{model: Persona}, {model: Archivo}]}}).then(function (solicitudes) {
                                if (solicitudes) {
                                    solicitudes.forEach(function (solicitudes, i) {
                                        carrera.forEach(function (lcarrera, j) {
                                            docente.forEach(function (ldocente, k) {
                                                if (solicitudes.tramite.external_docente === ldocente.external_id && solicitudes.tramite.external_carrera === lcarrera.external_id) {
                                                    Lista[i] = {
                                                        registro: solicitudes.tramite.registro,
                                                        solicitante: solicitudes.tramite.persona.nombres + " " + solicitudes.tramite.persona.apellidos,
                                                        revSolicitud: solicitudes.revSolicitud,
                                                        revSellos: solicitudes.revSellos,
                                                        asigDocente: solicitudes.asigDocente,
                                                        revDocumentacion: solicitudes.revDocumentacion,
                                                        estado: solicitudes.tramite.estado,
                                                        tipo: solicitudes.tramite.tipo,
                                                        cedula: solicitudes.tramite.persona.cedula,
                                                        external_id: solicitudes.external_id,
                                                        tramite: solicitudes.tramite.external_id,
                                                        carrera: lcarrera.nombre,
                                                        archivo: solicitudes.tramite.archivo.archivo,
                                                        cedula_docente: ldocente.persona.cedula,
                                                        docente: ldocente.persona.nombres + " " + ldocente.persona.apellidos,
                                                        foto: solicitudes.tramite.persona.foto
                                                    };
                                                }
                                            });

                                        });
                                    });
                                    //res.send(Lista);
                                    // res.send(seguimiento);
                                    res.render('partials/solicitante/index',
                                            {titulo: "Seguimiento",
                                                partial: 'solicitante/frm_seguimiento',
                                                nombre: req.user.nom,
                                                rol: req.user.rolN,
                                                foto: req.user.foto,
                                                idPersona: req.user.idPersona,
                                                Seguimiento: Lista,
                                                msg: {
                                                    error: req.flash('error'),
                                                    info: req.flash("info")
                                                }
                                            });
                                }
                            }).catch(function (err) {
                                req.flash('error', 'Hubo un error' + err);
                                res.redirect('/');
                            });
                        }
                    }).catch(function (err) {
                        req.flash('info', 'Hubo un error');
                        res.redirect('/');
                    });
                }

            }).catch(function (err) {
                req.flash('info', 'Hubo un error');
                res.redirect('/');
            });
        }

    }

}

module.exports = controladorSeguimiento;

