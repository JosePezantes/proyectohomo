'use strict';
var models = require('../models/');
const uuidv4 = require('uuid/v4');
var Persona = models.persona;
var Cuenta = models.cuenta;
var Docente = models.docente;
var Carrera = models.carrera;
/**
 * Clase que permite manipular los datos del modelo persona,  cuenta,docente, carrera
 */
class controladorDocente {
    /**
     * Funcion que permite mostrar la vista para visualizar los docentes 
     * @param {type} req el objeto peticion
     * @param {type} res el objeto respuesta
     * @returns {undefined} redirecciona a la pagina /lista/docente
     */
    listarDocente(req, res) {
        var Lista = {};
        Carrera.findAll({}).then(function (carrera) {
            console.log(carrera);
            if (carrera) {
                Docente.findAll({include: {model: Persona}}).then(function (docente) {
                    if (docente) {
                        carrera.forEach(function (lcarrera, i) {
                            docente.forEach(function (ldocente, j) {
                                if (ldocente.carrera === lcarrera.external_id) {
                                    Lista[j] = {
                                        foto: ldocente.persona.foto,
                                        docente: ldocente.persona.nombres + " " + ldocente.persona.apellidos,
                                        cedula: ldocente.persona.cedula,
                                        fecha_nacimiento: ldocente.persona.fecha_nacimiento,
                                        edad: ldocente.persona.edad,
                                        telefono: ldocente.persona.telefono,
                                        direccion: ldocente.persona.direccion,
                                        titulo: ldocente.titulo,
                                        carrera: lcarrera.nombre,
                                        external_id: ldocente.external_id
                                    }
                                }
                                ;
                            });
                        }
                        );
                        res.render('partials/decano/index',
                                {titulo: "Sistema de Homologacion",
                                    partial: 'decano/frm_docente',
                                    Docente: Lista,
                                    Carrera: carrera,
                                    nombre: req.user.nom,
                                    rol: req.user.rolN,
                                    foto: req.user.foto,
                                    msg: {
                                        error: req.flash('error'),
                                        info: req.flash("info")
                                    }
                                });
                    }
                }).catch(function (err) {
                    req.flash('info', 'Hubo un error');
                    res.redirect('/');
                });
            }
        }
        ).catch(function (err) {
            req.flash('info', 'Hubo un error');
            res.redirect('/');
        });
    }
    /**
     * Funcion que permite validar si un docente ya existe
     * @param {type} req el objeto peticion
     * @param {type} res el objeto respuesta
     * @returns {undefined} redirecciona a la pagina /lista/docente
     */
    buscarDocente(req, res) {
        var texto = req.params.texto;
        Docente.findOne({include: [{model: Persona, where: {cedula: texto}}]}).then(function (docente) {
            if (docente) {
                res.status(200).json(true);
            } else {
                Cuenta.findOne({where: {estado: true}, include: [{model: Persona, where: [{cedula: texto}, {id_rol: "3"}]}]}).then(function (cuenta) {
                    res.status(200).json(cuenta);
                });
            }
        });
    }
    /**
     * Funcion que permite cargar un docente para proceso de edicion
     * @param {type} req el objeto peticion
     * @param {type} res el objeto respuesta
     * @returns {undefined} redirecciona a la pagina /lista/docente
     */
    cargarDocente(req, res) {
        var texto = req.params.texto;
        Docente.findOne({where: {external_id: texto}, include: {model: Persona}}).then(function (docente) {
            res.status(200).json(docente);
        });

    }
    /**
     * Funcion que permite guardar y editar los docentes
     * @param {type} req el objeto peticion
     * @param {type} res el objeto respuesta
     * @returns {undefined} redirecciona a la pagina /lista/docente
     */
    guardarDocente(req, res) {

        if (req.body.externalD === "0") {
            Persona.findOne({where: {external_id: req.body.external}}).then(function (persona) {
                if (persona) {
                    console.log(persona);
                    Docente.create({
                        carrera: req.body.carrera,
                        titulo: req.body.titulo,
                        id_persona: persona.id,
                        external_id: uuidv4()
                    }).then(function (newDocente, created) {
                        console.log(newDocente);
                        if (newDocente) {
                            req.flash('info', 'Se a creado con exito');
                            res.redirect('/lista/docente');
                        }
                    });
                }
            }).catch(function (err) {
                req.flash('info', 'Hubo un error');
                res.redirect('/');
            });
        } else {
            Docente.update({
                carrera: req.body.carrera,
                titulo: req.body.titulo
            }, {where: {external_id: req.body.externalD}}).then(function (updatedDocente, created) {
                if (updatedDocente) {
                    req.flash('info', 'Se a modificado con exito');
                    res.redirect('/lista/docente');
                }
            });
        }
    }

}


module.exports = controladorDocente;
