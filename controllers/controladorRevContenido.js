'use strict';
var models = require('../models/');
const Sequelize = require('sequelize');
var root = require('app-root-path');
//para subir imagen
var fs = require('fs');
var maxFileSize = 10 * 1024 * 1024;
var extensiones = ["pdf"];
var formidable = require('formidable');

var Persona = models.persona;
var Tramite = models.tramite;
var Seguimiento = models.seguimiento;
var Archivo = models.archivo;
var Carrera = models.carrera;
var Docente = models.docente;
/**
 * Clase que permite manipular los datos del modelo persona, tramite, seguimiento, archivo, carrera, docente
 */
class controladorRevContenido {
    /**
     * Funcion que permite cargar un informe con los detalles de la revicion de documentacion por párte del docente
     * @param {type} req el objeto peticion
     * @param {type} res el objeto respuesta
     * @returns {undefined} redirecciona a la pagina /menu/tramite
     */
    revisionContenido(req, res) {
        var form = new formidable.IncomingForm();
        form.parse(req, function (err, fields, files) {
            if (files.archivo.size <= maxFileSize) {

                var extension = files.archivo.name.split(".").pop().toLowerCase();
                if (extension != "pdf") {
                    req.flash('error', 'Solo se admiten archivos de tipo .pdf');
                    res.redirect('/menu/tramite');
                } else {
                    if (extensiones.includes(extension)) {
                        var oldpath = files.archivo.path;

                        var nombre = fields.externalA + "." + extension;

                        fs.readFile(oldpath, function (err, data) {
                            // Write the file
                            fs.writeFile((root + "/public/upload/informesContenido/" + nombre), data, function (error) {
                                if (error)
                                    throw err;

                                Archivo.update({
                                    informe_peticionario: nombre,
                                }, {where: {external_id: fields.externalA}}).then(function (updatedArchivo, update) {
                                    if (updatedArchivo) {
                                        req.flash('info', 'Se ha registrado su operacion con exito');
                                        res.redirect('/menu/tramite');
                                    }

                                });
                            });
                            // Delete the file
                            fs.unlink(oldpath, function (err) {
                                if (err)
                                    throw err;
                            });
                        });
                    } else {
                        controladorRevContenido.eliminar(files.archivo.path);
                        console.log('Error de extencion');
                        req.flash('error', 'Error de extencion', false);
                        res.redirect('/menu/tramite' + fields.external);
                    }
                }
            } else {
                controladorRevContenido.eliminar(files.archivo.path);
                console.log('Error de tamaño se admite');
                req.flash('error', 'Error de tamaño se admite', +maxFileSize, false);
                res.redirect('/menu/tramite' + fields.external);
            }

        });

    }
    static eliminar(link) {
        fs.exists(link, function (exists) {
            if (exists) {
                console.log('files exists, deleting now ....' + link);
                fs.unlinkSync(link);
            } else {
                console.log('no se borro' + link);
            }
        });
    }
    /**
     * Funcion que permite editar el estado si el docente ya aprobo la revicion de documentacion
     * @param {type} req el objeto peticion
     * @param {type} res el objeto respuesta
     * @returns {undefined} redirecciona a la pagina /menu/tramite
     */
    editar(req, res) {
        Seguimiento.update({
            revDocumentacion: req.body.estado
        }, {where: {external_id: req.body.externalS}}).then(function (newSeguimiento, update) {
            if (newSeguimiento) {
                req.flash('info', 'Se ha registrado su operacion con exito');
                res.redirect('/menu/tramite');
            }
        }).catch(function (err) {
            req.flash('error', 'Hubo un error');
            res.redirect('/menu/tramite');
        });
    }
}

module.exports = controladorRevContenido;

