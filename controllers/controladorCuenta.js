'use strict';
var models = require('../models/');
var Persona = models.persona;
var Rol = models.rol;
var Cuenta = models.cuenta;
var Tramite = models.tramite;
var root = require('app-root-path');
//para subir imagen
var fs = require('fs');
var maxFileSize = 1 * 1024 * 1024;
var extensiones = ["jpg", "png", "jpeg"];
var formidable = require('formidable');
/**
 * Clase que permite manipular los datos del modelo persona, rol, cuenta,tramite
 */
class controladorCuenta {
    /**
     * Funcion que permite mostrar la vista para iniciar sesion
     * @param {type} req el objeto peticion
     * @param {type} res el objeto respuesta
     * @returns {undefined} redirecciona a la pagina /login
     */
    verLogin(req, res) {
        res.render('partials/login/login', {
            titulo: 'Inicio de Sesión',
            partial: 'login/frm_login',
            msg: {
                error: req.flash('error'),
                info: req.flash("info")
            }
        });
    }
    /**
     * Funcion que permite mostrar la vista para registrar usuario
     * @param {type} req el objeto peticion
     * @param {type} res el objeto respuesta
     * @returns {undefined} redirecciona a la pagina /registro
     */
    verRegistro(req, res) {
        res.render('partials/login/login', {
            titulo: 'Registro usuario',
            partial: 'login/frm_registro',
            msg: {
                error: req.flash('error'),
                info: req.flash("info")
            }
        });
    }
    /**
     * Funcion que permite mostrar la vista principal de cada rol con un mensaje de  BIENVENIDAA
     * @param {type} req el objeto peticion
     * @param {type} res el objeto respuesta
     * @returns {undefined} redirecciona a la pagina /perfil
     */
    verPerfil(req, res) {
        if (req.user.rolN === "Decano") {
            Cuenta.findOne({where: {id: req.user.idPersona}, include: [{model: Persona, include: {model: Rol}}]}).then(function (cuenta) {
                if (cuenta) {
                    res.render('partials/decano/index',
                            {titulo: "Sistema de Homologacion",
                                partial: 'decano/bienvenida',
                                usuario: cuenta,
                                nombre: req.user.nom,
                                rol: req.user.rolN,
                                correo: req.user.correo,
                                foto: req.user.foto,
                                id_cuenta: req.user.id_cuenta,
                                id_persona: req.user.id_persona,
                                idPersona: req.user.idPersona,
                                msg: {
                                    error: req.flash('error'),
                                    info: req.flash("info")
                                }
                            });
                }
            }).catch(function (err) {
                req.flash('info', 'Hubo un error');
                res.redirect('/');
            });
        } else if (req.user.rolN === "Abogado") {
            Cuenta.findOne({where: {id: req.user.idPersona}, include: [{model: Persona, include: {model: Rol}}]}).then(function (cuenta) {
                if (cuenta) {
                    res.render('partials/abogado/index',
                            {titulo: "Sistema de Homologacion",
                                partial: 'abogado/bienvenida',
                                usuario: cuenta,
                                nombre: req.user.nom,
                                rol: req.user.rolN,
                                correo: req.user.correo,
                                foto: req.user.foto,
                                id_cuenta: req.user.id_cuenta,
                                id_persona: req.user.id_persona,
                                idPersona: req.user.idPersona,
                                msg: {
                                    error: req.flash('error'),
                                    info: req.flash("info")
                                }
                            });
                }
            }).catch(function (err) {
                req.flash('info', 'Hubo un error');
                res.redirect('/');
            });
        } else if (req.user.rolN === "Solicitante") {
            Cuenta.findOne({where: {id: req.user.idPersona}, include: [{model: Persona, include: {model: Rol}}]}).then(function (cuenta) {
                if (cuenta) {
                    res.render('partials/solicitante/index',
                            {titulo: "Sistema de Homologacion",
                                partial: 'solicitante/bienvenida',
                                usuario: cuenta,
                                nombre: req.user.nom,
                                rol: req.user.rolN,
                                correo: req.user.correo,
                                foto: req.user.foto,
                                id_cuenta: req.user.id_cuenta,
                                id_persona: req.user.id_persona,
                                idPersona: req.user.idPersona,
                                msg: {
                                    error: req.flash('error'),
                                    info: req.flash("info")
                                }
                            });
                }
            }).catch(function (err) {
                req.flash('info', 'Hubo un error');
                res.redirect('/');
            });
        } else if (req.user.rolN === "Docente") {
            Cuenta.findOne({where: {id: req.user.idPersona}, include: [{model: Persona, include: {model: Rol}}]}).then(function (cuenta) {
                console.log(cuenta);
                if (cuenta) {
                    res.render('partials/docente/index',
                            {titulo: "Sistema de Homologacion",
                                partial: 'docente/bienvenida',
                                usuario: cuenta,
                                correo: req.user.correo,
                                nombre: req.user.nom,
                                rol: req.user.rolN,
                                foto: req.user.foto,
                                id_cuenta: req.user.id_cuenta,
                                id_persona: req.user.id_persona,
                                idPersona: req.user.idPersona,
                                msg: {
                                    error: req.flash('error'),
                                    info: req.flash("info")
                                }
                            });
                }
            }).catch(function (err) {
                req.flash('info', 'Hubo un error');
                res.redirect('/');
            });
        }

    }
    /**
     * Funcion que permite mostrar la vista para visualizar los usuarios registrados
     * @param {type} req el objeto peticion
     * @param {type} res el objeto respuesta
     * @returns {undefined} redirecciona a la pagina /lista/registro
     */
    verCuenta(req, res) {
        Rol.findAll({}).then(function (rol) {
            Cuenta.findAll({include: [{model: Persona, include: {model: Rol}}]}).then(function (cuenta) {
                if (cuenta) {
                    res.render('partials/decano/index',
                            {titulo: "Sistema de Homologacion",
                                partial: 'decano/frm_usuarios',
                                listaR: rol,
                                nombre: req.user.nom,
                                rol: req.user.rolN,
                                listaC: cuenta,
                                foto: req.user.foto,
                                msg: {
                                    error: req.flash('error'),
                                    info: req.flash("info")
                                }
                            });
                }
            }).catch(function (err) {
                req.flash('error', 'Hubo un error');
                res.redirect('/');
            });
        }).catch(function (err) {
            req.flash('error', 'Hubo un error');
            res.redirect('/');
        });
    }
    /**
     * Funcion que permite dar de baja a las cuentas
     * @param {type} req el objeto peticion
     * @param {type} res el objeto respuesta
     * @returns {undefined} redirecciona a la pagina /lista/registro
     */
    bloquearCuenta(req, res) {
        Cuenta.update({
            estado: req.body.estado
        }, {where: {external_id: req.body.externalC}}).then(function (updatedCuenta, created) {
            if (updatedCuenta) {
                req.flash('info', 'Se ha modificado correctamente');
                res.redirect('/lista/registro');
            }
        });
    }
    /**
     * Funcion que permite dar un rol a cada usuario
     * @param {type} req el objeto peticion
     * @param {type} res el objeto respuesta
     * @returns {undefined} redirecciona a la pagina /lista/registro
     */
    rolCuenta(req, res) {
        Persona.update({
            id_rol: req.body.rol
        }, {where: {external_id: req.body.externalR}}).then(function (updatedCuenta, created) {
            if (updatedCuenta) {
                req.flash('info', 'Se ha modificado correctamente');
                res.redirect('/lista/registro');
            }
        });
    }
    /**
     * Funcion que permite destruir todas las variables de sesion
     * @param {type} req el objeto peticion
     * @param {type} res el objeto respuesta
     * @returns {undefined} redirecciona a la pagina /
     */
    cerrar(req, res) {
        req.session.destroy();
        res.redirect("/");
    }
    /**
     * Funcion que permite mostrar los datos del usuario autentificado
     * @param {type} req el objeto peticion
     * @param {type} res el objeto respuesta
     * @returns {undefined} redirecciona a la pagina /perfil/usuario
     */
    ListarPerfil(req, res) {
        if (req.user.rolN === "Decano") {
            Cuenta.findOne({where: {id: req.user.idPersona}, include: [{model: Persona, include: {model: Rol}}]}).then(function (cuenta) {
                if (cuenta) {
                    res.render('partials/decano/index',
                            {titulo: "Sistema de Homologacion",
                                partial: 'decano/frm_perfil',
                                usuario: cuenta,
                                foto: req.user.foto,
                                correo: req.user.correo,
                                nombre: req.user.nom,
                                rol: req.user.rolN,
                                id_cuenta: req.user.id_cuenta,
                                id_persona: req.user.id_persona,
                                msg: {
                                    error: req.flash('error'),
                                    info: req.flash("info")
                                }
                            });
                }
            }).catch(function (err) {
                req.flash('error', 'Hubo un error');
                res.redirect('/');
            });
        } else if (req.user.rolN === "Abogado") {
            Cuenta.findOne({where: {id: req.user.idPersona}, include: [{model: Persona, include: {model: Rol}}]}).then(function (cuenta) {
                if (cuenta) {
                    res.render('partials/abogado/index',
                            {titulo: "Sistema de Homologacion",
                                partial: 'abogado/frm_perfil',
                                usuario: cuenta,
                                foto: req.user.foto,
                                correo: req.user.correo,
                                nombre: req.user.nom,
                                rol: req.user.rolN,
                                id_cuenta: req.user.id_cuenta,
                                id_persona: req.user.id_persona,
                                msg: {
                                    error: req.flash('error'),
                                    info: req.flash("info")
                                }
                            });
                }
            }).catch(function (err) {
                req.flash('error', 'Hubo un error');
                res.redirect('/');
            });
        } else if (req.user.rolN === "Docente") {
            Cuenta.findOne({where: {id: req.user.idPersona}, include: [{model: Persona, include: {model: Rol}}]}).then(function (cuenta) {
                if (cuenta) {
                    res.render('partials/docente/index',
                            {titulo: "Sistema de Homologacion",
                                partial: 'docente/frm_perfil',
                                usuario: cuenta,
                                foto: req.user.foto,
                                correo: req.user.correo,
                                nombre: req.user.nom,
                                rol: req.user.rolN,
                                id_cuenta: req.user.id_cuenta,
                                id_persona: req.user.id_persona,
                                msg: {
                                    error: req.flash('error'),
                                    info: req.flash("info")
                                }
                            });
                }
            }).catch(function (err) {
                req.flash('error', 'Hubo un error');
                res.redirect('/');
            });
        } else if (req.user.rolN === "Solicitante") {
            Tramite.findAll({where: {id_persona: req.user.idPersona}}).then(function (tramite) {
                if (tramite) {
                    Cuenta.findOne({where: {id: req.user.idPersona}, include: [{model: Persona, include: {model: Rol}}]}).then(function (cuenta) {
                        if (cuenta) {
                            res.render('partials/solicitante/index',
                                    {titulo: "Sistema de Homologacion",
                                        partial: 'solicitante/frm_perfil',
                                        usuario: cuenta,
                                        Tramite: tramite,
                                        correo: req.user.correo,
                                        foto: req.user.foto,
                                        nombre: req.user.nom,
                                        rol: req.user.rolN,
                                        id_cuenta: req.user.id_cuenta,
                                        id_persona: req.user.id_persona,
                                        msg: {
                                            error: req.flash('error'),
                                            info: req.flash("info")
                                        }

                                    });
                        }
                    }).catch(function (err) {
                        req.flash('error', 'Hubo un error');
                        res.redirect('/');
                    });
                }
            }).catch(function (err) {
                req.flash('error', 'Hubo un error');
                res.redirect('/');
            });
        }
    }
    /**
     * Funcion que permite guardar la foto del usuario logeado
     * @param {type} req el objeto peticion
     * @param {type} res el objeto respuesta
     * @returns {undefined} redirecciona a la pagina /perfil/foto
     */
    guardarFoto(req, res, next) {
        console.log("estoy aki perro");
        var form = new formidable.IncomingForm();
        form.parse(req, function (err, fields, files) {
            console.log('+++++++++++++++++');
            if (files.archivo.size <= maxFileSize) {
                console.log('****************' + root);
                var extension = files.archivo.name.split(".").pop().toLowerCase();
                if (extensiones.includes(extension)) {
                    var oldpath = files.archivo.path;
                    console.log('por que no entra');
                    var nombre = fields.external + "." + extension;
                    console.log(files.archivo.path);
                    fs.readFile(oldpath, function (err, data) {
                        // Write the file
                        fs.writeFile((root + "/public/images/gallery/" + nombre), data, function (error) {
                            if (error)
                                throw err;
                            console.log('File written!');
                            Persona.update({
                                foto: nombre
                            }, {where: {external_id: fields.external}}).then(function (updatedPersona, created) {
                                if (updatedPersona) {
                                    req.flash('info', 'se subio correctamente', false);
                                    res.redirect('/perfil/usuario');
                                }

                            });
                        });
                        // Delete the file
                        fs.unlink(oldpath, function (err) {
                            if (err)
                                throw err;
                            console.log('File deleted!');
                        });
                    });
                } else {
                    controladorCuenta.eliminar(files.archivo.path);
                    console.log('Error de extencion');
                    req.flash('error', 'Error de extencion', false);
                    res.redirect('/perfil/usuario' + fields.external);
                }
            } else {
                controladorCuenta.eliminar(files.archivo.path);
                console.log('Error de tamaño se admite');
                req.flash('error', 'Error de tamaño se admite', +maxFileSize, false);
                res.redirect('/perfil/usuario' + fields.external);
            }
        });
    }
    static eliminar(link) {
        fs.exists(link, function (exists) {
            if (exists) {
                console.log('files exists, deleting now ....' + link);
                fs.unlinkSync(link);
            } else {
                console.log('no se borro' + link);
            }
        });
    }
    /**
     * Funcion que permite validar que el numero de cedula no se repita con otro ya existente
     * @param {type} req el objeto peticion
     * @param {type} res el objeto respuesta
     * @returns {undefined} retorna un json
     */
    CedulaUnica(req, res) {
        var texto = req.params.texto;
        Persona.findOne({where: {cedula: texto}}).then(function (cedula) {
            if (cedula) {
                res.status(200).json(true);
            } else {
                res.status(200).json(false);
            }

        });
    }

}


module.exports = controladorCuenta;
