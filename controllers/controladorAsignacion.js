'use strict';
var models = require('../models/');
var Persona = models.persona;
var Tramite = models.tramite;
var Seguimiento = models.seguimiento;
var Archivo = models.archivo;
var Carrera = models.carrera;
var Docente = models.docente;
/**
 * Clase que permite manipular los datos del modelo persona, tramite, seguimiento,archivo,carrera,docente
 */

class controladorAsignacion {
    /**
     * Funcion que permite mostrar la vista para la asignacion de Docentes
     * @param {type} req el objeto peticion
     * @param {type} res el objeto respuesta
     * @returns {undefined} redirecciona a la pagina /listar/asignacion
     */
    visualizarAsignaciones(req, res) {
        var Lista = {};
        Carrera.findAll({}).then(function (carrera) {
            if (carrera) {
                Docente.findAll({include: {model: Persona}}).then(function (docente) {
                    if (docente) {
                        Seguimiento.findAll({where: {revSellos: "1"}, include: {model: Tramite, include: [{model: Persona}, {model: Archivo}]}}).then(function (solicitudes) {
                            if (solicitudes) {
                                solicitudes.forEach(function (solicitudes, i) {
                                    carrera.forEach(function (lcarrera, j) {
                                        docente.forEach(function (ldocente, k) {
                                            if (solicitudes.tramite.external_docente === ldocente.external_id && solicitudes.tramite.external_carrera === lcarrera.external_id) {
                                                Lista[i] = {
                                                    registro: solicitudes.tramite.registro,
                                                    solicitante: solicitudes.tramite.persona.nombres + " " + solicitudes.tramite.persona.apellidos,
                                                    estado: solicitudes.asigDocente,
                                                    tipo: solicitudes.tramite.tipo,
                                                    cedula: solicitudes.tramite.persona.cedula,
                                                    external_id: solicitudes.external_id,
                                                    tramite: solicitudes.tramite.external_id,
                                                    carrera: lcarrera.nombre,
                                                    archivo: solicitudes.tramite.archivo.archivo,
                                                    cedula_docente: ldocente.persona.cedula,
                                                    docente: ldocente.persona.nombres + " " + ldocente.persona.apellidos,
                                                    foto: solicitudes.tramite.persona.foto
                                                };
                                            }
                                        });
                                    });
                                });
                                res.render('partials/decano/index',
                                        {titulo: "Asigancion de Docentes",
                                            partial: 'decano/frm_asignacion',
                                            nombre: req.user.nom,
                                            rol: req.user.rolN,
                                            foto: req.user.foto,
                                            idPersona: req.user.idPersona,
                                            Asignacion: Lista,
                                            msg: {
                                                error: req.flash('error'),
                                                info: req.flash("info")
                                            }
                                        });
                            }
                        }).catch(function (err) {
                            req.flash('error', 'Hubo un error' + err);
                            res.redirect('/');
                        });
                    }
                }).catch(function (err) {
                    req.flash('info', 'Hubo un error');
                    res.redirect('/');
                });
            }
        }).catch(function (err) {
            req.flash('info', 'Hubo un error');
            res.redirect('/');
        });
    }
    /**
     * Funcion que permite buscar un docente por medio de la cedula
     * @param {type} req el objeto peticion
     * @param {type} res el objeto respuesta
     * @returns {undefined} retorna un json con el docente especificado
     */
    cargarDocente(req, res) {
        var texto = req.params.texto;
        Docente.findOne({include: {model: Persona, where: {cedula: texto}}}).then(function (docente) {
            res.status(200).json(docente);
        });
    }
    /**
     * Funcion que permite asignar un docente para el proceso de homologacion
     * @param {type} req el objeto peticion
     * @param {type} res el objeto respuesta
     * @returns {undefined} redirecciona a la pagina /listar/asignacion
     */
    asignarDocente(req, res) {
        Seguimiento.update({
            asigDocente: req.body.estado
        }, {where: {external_id: req.body.external}}).then(function (updatedSeguimiento, created) {
            if (updatedSeguimiento) {
                Tramite.update({
                    external_docente: req.body.externalD
                }, {where: {external_id: req.body.tramite}}).then(function (updatedTramite, created) {
                    if (updatedTramite) {
                        req.flash('info', 'Operacion exitosa');
                        res.redirect('/listar/asignacion');
                    }
                });
            }
        });
    }
}

module.exports = controladorAsignacion;

