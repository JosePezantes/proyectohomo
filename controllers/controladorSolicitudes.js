'use strict';
var models = require('../models/');
var Persona = models.persona;
var Tramite = models.tramite;
var Seguimiento = models.seguimiento;
var Archivo = models.archivo;
var Carrera = models.carrera;
/**
 * Clase que permite manipular los datos del modelo persona, tramite, seguimiento, archivo, carrera
 */
class controladorSolicitudes {
    /**
     * Funcion que permite visulizar la vista con las solicitudes para la homologacion
     * @param {type} req el objeto peticion
     * @param {type} res el objeto respuesta
     * @returns {undefined} redirecciona a la pagina /menu/solicitudes
     */
    visualizarSolicitudes(req, res) {
        var Lista = {};
        Carrera.findAll({}).then(function (carrera) {
            if (carrera) {
                Seguimiento.findAll({include: {model: Tramite, include: [{model: Persona}, {model: Archivo}]}}).then(function (solicitudes) {
                    if (solicitudes) {
                        solicitudes.forEach(function (solicitudes, i) {
                            carrera.forEach(function (lcarrera, j) {
                                if (solicitudes.tramite.external_carrera === lcarrera.external_id) {
                                    Lista[i] = {
                                        registro: solicitudes.tramite.registro,
                                        solicitante: solicitudes.tramite.persona.nombres + " " + solicitudes.tramite.persona.apellidos,
                                        estado: solicitudes.revSolicitud,
                                        tipo: solicitudes.tramite.tipo,
                                        cedula: solicitudes.tramite.persona.cedula,
                                        external_id: solicitudes.external_id,
                                        carrera: lcarrera.nombre,
                                        archivo: solicitudes.tramite.archivo.archivo,
                                        foto: solicitudes.tramite.persona.foto
                                    };
                                }
                            });
                        });

                        res.render('partials/decano/index',
                                {titulo: "Seguimiento",
                                    partial: 'decano/frm_solicitudes',
                                    nombre: req.user.nom,
                                    rol: req.user.rolN,
                                    foto: req.user.foto,
                                    idPersona: req.user.idPersona,
                                    Solicitud: Lista,
                                    msg: {
                                        error: req.flash('error'),
                                        info: req.flash("info")
                                    }
                                });
                    }
                }).catch(function (err) {
                    req.flash('error', 'Hubo un error' + err);
                    res.redirect('/');
                });
            }

        }).catch(function (err) {
            req.flash('info', 'Hubo un error');
            res.redirect('/');
        });
    }
    /**
     * Funcion que permite registrar la aceptacion de las solicitudes
     * @param {type} req el objeto peticion
     * @param {type} res el objeto respuesta
     * @returns {undefined} redirecciona a la pagina /menu/solicitudes
     */
    sumillarSolicitud(req, res) {
        Seguimiento.update({
            revSolicitud: req.body.estado
        }, {where: {external_id: req.body.external}}).then(function (updatedCuenta, created) {
            if (updatedCuenta) {
                req.flash('info', 'Se ha sumillado correctamente');
                res.redirect('/menu/solicitudes');
            }
        });
    }
}

module.exports = controladorSolicitudes;

