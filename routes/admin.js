var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function (req, res, next) {
    //modelos
    var model = require('./../models/');
    model.sequelize.sync().then(() => {
        console.log('Se ha conectado a la db');
        res.send('Se a sincronizado la DB');
        require('../controllers/init');
    }).catch(err => {
        console.log(err, "Hubo un error");
        res.send('No se pudo sincronizar la DB');
    });
});



module.exports = router;

