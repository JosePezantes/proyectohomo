var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var bodyParser = require('body-parser');



var exphbs = require('express-handlebars');
var session = require('express-session');
var flash = require('connect-flash');
var passport = require('passport');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var usersAdmin = require('./routes/admin');

var app = express();

//for bodyParser
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

//passport
app.use(session({secret: 'Citas Medicas', resave: true, saveUninitialized: true}));
app.use(passport.initialize());
app.use(passport.session());

//Handlebars motor de plantilla
app.set('views', './views');
app.engine('hbs', exphbs({
    extname: '.hbs'
}));
app.set('view engine', '.hbs');

//Configuracion express-session
app.use(session({
    secret: 'Cuarto B',
    resave: false,
    saveUninitialized: true
}));

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
//Configuracion connect-flash
app.use(flash());
//Configuracion passport
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
require('./config/passport')(passport);

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/admin', usersAdmin);

var models = require('./models/');
//INSERT INICIAL
//require('./controllers/init/init');

//ESTRATEGIAS PASSPORT
require('./config/passport.js')(passport);


//catch 404 and forward to error handler
app.use(function (req, res, next) {
    res.status(404).render('404', {title: "Sorry, page not found", session: req.sessionbo});
});

app.use(function (req, res, next) {
    res.status(500).render('404', {title: "Sorry, page not found"});
});


module.exports = app;

