'use strict';
module.exports = (sequelize, DataTypes) => {
    const carrera = sequelize.define('carrera', {
        area: DataTypes.STRING,
        nombre: DataTypes.STRING,
        external_id: DataTypes.UUID
    }, {freezeTableName: true});
    carrera.associate = function (models) {
        // associations can be defined here
        carrera.hasOne(models.mallaCurricular, {foreignKey: 'id_carrera', as: 'mallaCurricular'});
    };
    return carrera;
};


