'use strict';
module.exports = (sequelize, DataTypes) => {
    const ciclo = sequelize.define('ciclo', {
        numero: DataTypes.INTEGER,
        nombre: DataTypes.STRING
    }, {freezeTableName: true});

    ciclo.associate = function (models) {
        // associations can be defined here
        ciclo.belongsTo(models.mallaCurricular, {foreignKey: 'id_mallaCurricular'});
        ciclo.hasMany(models.materia, {foreignKey: 'id_ciclo', as: 'materia'});
    };

    return ciclo;
};
