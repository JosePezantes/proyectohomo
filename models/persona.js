'use strict';
module.exports = (sequelize, DataTypes) => {
    const persona = sequelize.define('persona', {
        cedula: DataTypes.STRING,
        apellidos: DataTypes.STRING,
        nombres: DataTypes.STRING,
        edad: DataTypes.INTEGER,
        fecha_nacimiento: DataTypes.DATEONLY,
        direccion: DataTypes.STRING,
        telefono: DataTypes.STRING,
        foto: DataTypes.STRING,
        external_id: DataTypes.UUID
    }, {freezeTableName: true});

    persona.associate = function (models) {
        // associations can be defined here
        persona.hasOne(models.cuenta, {foreignKey: 'id_persona', as: 'cuenta'});
        persona.belongsTo(models.rol, {foreignKey: 'id_rol'});
        persona.hasOne(models.docente, {foreignKey: 'id_persona', as: 'docente'});
        persona.hasMany(models.tramite, {foreignKey: 'id_persona', as: 'tramite'});
    };

    return persona;
};
