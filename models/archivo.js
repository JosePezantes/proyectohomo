'use strict';
/**
 * modelo archivo
 */
module.exports = (sequelize, DataTypes) => {
    const archivo = sequelize.define('archivo', {
        archivo: DataTypes.STRING,
        informe_peticionario: DataTypes.STRING,
        external_id: DataTypes.UUID
    }, {freezeTableName: true});
    /**
     * relasiones
     */
    archivo.associate = function (models) {
        // associations can be defined here

        archivo.hasMany(models.tramite, {foreignKey: 'id_archivo', as: 'tramite'});
    };

    return archivo;
};