'use strict';
module.exports = (sequelize, DataTypes) => {
    const materia = sequelize.define('materia', {
        nombre: DataTypes.STRING,
        creditos: DataTypes.INTEGER,
        codigo: DataTypes.STRING,
        duracion: DataTypes.INTEGER,
        obligatoria: DataTypes.BOOLEAN,
        external_id: DataTypes.UUID
    }, {freezeTableName: true});

    materia.associate = function (models) {
        // associations can be defined here
        materia.hasOne(models.silabo, {foreignKey: 'id_materia', as: 'silabo'});
        materia.belongsTo(models.ciclo, {foreignKey: 'id_ciclo'});
    };

    return materia;
};

