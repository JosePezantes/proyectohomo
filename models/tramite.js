'use strict';
module.exports = (sequelize, DataTypes) => {
    const tramite = sequelize.define('tramite', {
        registro: DataTypes.STRING,
        estado: DataTypes.BOOLEAN,
        external_docente: DataTypes.STRING,
        external_carrera: DataTypes.STRING,
        tipo: DataTypes.STRING,
        external_id: DataTypes.UUID
    }, {freezeTableName: true});
    tramite.associate = function (models) {
        // associations can be defined here
        tramite.belongsTo(models.persona, {foreignKey: 'id_persona'});
        tramite.belongsTo(models.archivo, {foreignKey: 'id_archivo'});
        tramite.hasMany(models.seguimiento, {foreignKey: 'id_tramite', as: 'seguimiento'});
    };

    return tramite;
};                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      