'use strict';
module.exports = (sequelize, DataTypes) => {
    const docente = sequelize.define('docente', {
        titulo: DataTypes.STRING,
        carrera: DataTypes.STRING,
        external_id: DataTypes.UUID
    }, {freezeTableName: true});

    docente.associate = function (models) {
        // associations can be defined here
        docente.belongsTo(models.persona, {foreignKey: 'id_persona'});
        docente.hasMany(models.silabo, {foreignKey: 'id_docente', as: 'silabo'});
    };

    return docente;
};


